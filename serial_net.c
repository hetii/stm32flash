#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "serial.h"
#include "port.h"

struct serial {
  int fd;
  char setup_str[11];
};

static serial_t *serial_tcp_open(const char *host, unsigned int port){
  static struct sockaddr_in server;
  serial_t *h = calloc(sizeof(serial_t), 1);

    //Create socket
    h->fd = socket(AF_INET , SOCK_STREAM , 0);
    if (h->fd == -1){
        printf("Could not create socket");
    }
    puts("Socket created");
     
    server.sin_addr.s_addr = inet_addr(host);
    server.sin_family = AF_INET;
    server.sin_port = htons(port);
 
    //Connect to remote server
    if (connect(h->fd, (struct sockaddr *)&server , sizeof(server)) < 0){
        perror("connect failed. Error");
        free(h);
        return NULL;
    }
     
    puts("Connected\n");
  return h;
}

static void serial_tcp_close(serial_t *h){
  close(h->fd);
  free(h);
}

static port_err_t serial_net_open(struct port_interface *port, struct port_options *ops){
  serial_t *h;

  /* 1. check options */
  if (ops->baudRate == SERIAL_BAUD_INVALID)
    return PORT_ERR_UNKNOWN;
  if (serial_get_bits(ops->serial_mode) == SERIAL_BITS_INVALID)
    return PORT_ERR_UNKNOWN;
  if (serial_get_parity(ops->serial_mode) == SERIAL_PARITY_INVALID)
    return PORT_ERR_UNKNOWN;
  if (serial_get_stopbit(ops->serial_mode) == SERIAL_STOPBIT_INVALID)
    return PORT_ERR_UNKNOWN;

  /* 2. open it */
  h = serial_tcp_open(ops->device, 23);

  if (h == NULL)
    return PORT_ERR_UNKNOWN;

  /* 4. set options */
/*  if (serial_setup(h, ops->baudRate,
       serial_get_bits(ops->serial_mode),
       serial_get_parity(ops->serial_mode),
       serial_get_stopbit(ops->serial_mode)
      ) != PORT_ERR_OK) {
    serial_tcp_close(h);
    return PORT_ERR_UNKNOWN;
  }
*/
  port->private = h;
  return PORT_ERR_OK;
}

static port_err_t serial_net_close(struct port_interface *port){
  serial_t *h;

  h = (serial_t *)port->private;
  if (h == NULL)
    return PORT_ERR_UNKNOWN;

  serial_tcp_close(h);
  port->private = NULL;
  return PORT_ERR_OK;
}

static port_err_t serial_net_read(struct port_interface *port, void *buf, size_t nbyte){
  serial_t *h;
  ssize_t r;
  uint8_t *pos = (uint8_t *)buf;

  h = (serial_t *)port->private;
  if (h == NULL)
    return PORT_ERR_UNKNOWN;

  while (nbyte) {
     //Receive a reply from the server
        r = recv(h->fd, pos, nbyte, 0);
    //r = read(h->fd, pos, nbyte);

    if (r == 0)
      return PORT_ERR_TIMEDOUT;
    if (r < 0)
      return PORT_ERR_UNKNOWN;

    nbyte -= r;
    pos += r;
  }
  return PORT_ERR_OK;
}

static port_err_t serial_net_write(struct port_interface *port, void *buf, size_t nbyte){
  serial_t *h;
  ssize_t r;
  const uint8_t *pos = (const uint8_t *)buf;

  h = (serial_t *)port->private;
  if (h == NULL)
    return PORT_ERR_UNKNOWN;

  while (nbyte) {
    r = send(h->fd , pos, nbyte, 0);
    if (r < 0)
      return PORT_ERR_UNKNOWN;
    nbyte -= r;
    pos += r;
  }
  return PORT_ERR_OK;
}

static port_err_t serial_net_gpio(struct port_interface *port, serial_gpio_t n, int level){
  return PORT_ERR_OK;
}

static const char *serial_net_get_cfg_str(struct port_interface *port){
  serial_t *h;
  h = (serial_t *)port->private;
  return h ? h->setup_str : "INVALID";
}

static port_err_t serial_net_flush(struct port_interface *port){
  serial_t *h;
  h = (serial_t *)port->private;
  if (h == NULL)
    return PORT_ERR_UNKNOWN;

  //serial_flush(h);
  return PORT_ERR_OK;
}

struct port_interface port_serial = {
  .name = "serial_net",
  .flags  = PORT_BYTE | PORT_GVR_ETX | PORT_CMD_INIT | PORT_RETRY,
  .open = serial_net_open,
  .close  = serial_net_close,
  .flush  = serial_net_flush,
  .read = serial_net_read,
  .write  = serial_net_write,
  .gpio = serial_net_gpio,
  .get_cfg_str = serial_net_get_cfg_str,
};
